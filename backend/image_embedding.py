import torch
from transformers import CLIPProcessor, CLIPModel
from PIL import Image
import numpy as np

def images_to_embeddings(image_paths):
    """    
    Args:
    - image_paths (list): List of file paths of images.
    
    Returns:
    - embeddings (np.ndarray): Array of vector embeddings.
    """
    # Load pre-trained CLIP model
    model_name = "openai/clip-vit-base-patch32"
    model = CLIPModel.from_pretrained(model_name)
    processor = CLIPProcessor.from_pretrained(model_name)

    # Initialize an empty list to store embeddings
    embeddings = []

    for path in image_paths:
        # Load and preprocess image
        img = Image.open(path)
        inputs = processor(images=img, return_tensors="pt")

        # Generate embedding for the image
        with torch.no_grad():
            image_features = model.get_image_features(**inputs)
            embedding = image_features.cpu().numpy().flatten()
        
        # Append the embedding to the list
        embeddings.append(embedding)

    # Convert list of embeddings to numpy array
    embeddings = np.array(embeddings)

    return embeddings
