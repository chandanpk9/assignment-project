from image_embedding import images_to_embeddings
from langchain_pinecone import PineconeVectorStore

# print(os.environ['PINECONE_API_KEY'])

# Example usage
image_paths = ['./test-images/img1.png', './test-images/img2.png']  # List of image file paths
image_embeddings = images_to_embeddings(image_paths)
print(image_embeddings) # resulten array of images embedding



# Example embeddings and metadata
image_embeddings = image_embeddings  # List of image embeddings (vectors)
metadata = "this are some of the screenshots"  # Single metadata value for all images

# Serialize embeddings and metadata
serialized_data = [
    {"id": str(i), "embedding": embedding, "metadata": metadata}
    for i, embedding in enumerate(image_embeddings)
]

vectorstore = PineconeVectorStore.from_documents(serialized_data, index_name="image-search")
