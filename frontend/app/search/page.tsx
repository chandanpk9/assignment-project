"use client";

import { useState } from "react";
import Link from "next/link";

export default function InferenceInterface() {
  const [image, setImage] = useState<File | null>(null);
  const [description, setDescription] = useState("");

  const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (file) {
      setImage(file);
    }
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // Submission logic pending until backend is ready
  };

  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <main className="flex flex-col items-center justify-center w-full flex-1 px-20 text-center">
        <Link className="underline text-blue-800 mb-6" href={"/learning"}>
          Go to Learning interface
        </Link>
        <h1 className="text-4xl font-bold">Inference Interface</h1>
        <form className="mt-8" onSubmit={handleSubmit}>
          <div className="mb-4">
            <label htmlFor="image" className="block text-lg font-medium mb-2">
              Upload Image
            </label>
            <input
              type="file"
              id="image"
              accept="image/*"
              onChange={handleImageChange}
              className="py-2 px-4 border rounded-md w-full"
            />
          </div>
          <button
            type="submit"
            className="py-2 px-6 bg-blue-600 text-white font-bold rounded-md hover:bg-blue-700 transition duration-300"
          >
            Submit
          </button>
        </form>
        {description && (
          <div className="mt-8">
            <h2 className="text-2xl font-bold mb-2">Description</h2>
            <p className="text-lg">{description}</p>
          </div>
        )}
      </main>
    </div>
  );
}
