import Link from "next/link";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className="flex w-1/5 justify-between text-blue-800 underline">
        <Link href={"/learning"}>Learning Interface</Link>
        <Link href={"/search"}>Inference Interface</Link>
      </div>
    </main>
  );
}
