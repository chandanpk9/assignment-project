"use client";

import Link from "next/link";
import { useState } from "react";

export default function LearningInterface() {
  const [image, setImage] = useState<File | null>(null);
  const [metadata, setMetadata] = useState<any>("");

  const handleImageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    console.log(e.target.files);
    if (file) {
      setImage(file);
    }
  };

  const handleMetadataChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setMetadata(e.target.value);
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    console.log("Submitting:", image, metadata);
  };

  return (
    <div className="flex flex-col items-center justify-center h-[100vh] py-2">
      <div className="flex flex-col items-center justify-center w-full px-20 text-center">
        <Link className="underline text-blue-800 mb-6" href={"/search"}>
          Go to Inference Interface
        </Link>
        <h1 className="text-4xl font-bold">Learning Interface</h1>
        <form className="mt-8" onSubmit={handleSubmit}>
          <div className="mb-4">
            <label htmlFor="image" className="block text-lg font-medium mb-2">
              Upload Image
            </label>
            <input
              type="file"
              id="image"
              accept=".png,.jpg,.jpeg"
              onChange={handleImageChange}
              className="py-2 px-4 border rounded-sm w-full"
            />
          </div>
          <input
            type="text"
            value={metadata}
            onChange={handleMetadataChange}
            className="border py-2 px-4 rounded-sm w-[100%] mb-4"
            placeholder="About the image"
          />
          <button
            type="submit"
            className="py-2 px-6 bg-blue-600 text-white font-bold rounded-sm hover:bg-blue-700"
          >
            Submit
          </button>
        </form>
      </div>
    </div>
  );
}
